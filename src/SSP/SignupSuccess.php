<?php
namespace SSP;

use Silex\Application;
use Symfony\Component\HttpFoundation\Request;

class SignupSuccess
{

	public static function load(Request $request, Application $app) {

		//Set Details
		$twig_vars['customer'] = $request->get("customer");
		$twig_vars['api_key'] = $request->get("api_key");

		return $app['twig']->render('signup-success.twig', $twig_vars);
	}

}

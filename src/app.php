<?php
//Register Twig
$app->register(new Silex\Provider\TwigServiceProvider(), [
	'debug' => true,
	'twig.path' => '../views',
	'twig.options' => [
		"autoescape" => false,
	],
]);

//Load Config Details
require("config.php");
$app['my_config'] = $config;

//Setup AWS
$app['aws'] = new \Aws\Sdk($app['my_config']['aws_credentials']);
$app['aws_s3'] = new \Aws\S3\S3Client($app['my_config']['aws_credentials']);

//Set Timezone
date_default_timezone_set("UTC");
$app['twig']->getExtension('Twig_Extension_Core')->setTimezone("UTC");

//That's All....
return $app;

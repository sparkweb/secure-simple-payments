<?php
//include("maint.php");
error_reporting(E_ALL);
ini_set('log_errors', 'On');
ini_set('display_errors', 'On');

require __DIR__ . '/../vendor/autoload.php';

$app = new Silex\Application();

require __DIR__ . '/../src/app.php';
require __DIR__ . '/../src/controllers.php';

$app->run();

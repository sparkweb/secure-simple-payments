<?php
namespace SSP\Services;

use Silex\Application;
use Aws\DynamoDb\Exception\DynamoDbException;
use Aws\DynamoDb\Marshaler;

class DynamoDbMapping
{

	private $app;
	private $table_name = "securesimplepayments";
	public $error;

	public function __construct(Application $app) {
		$this->app = $app;
	}


	public function checkForAccount(string $name): bool {
		$this->error = "";
		$arr = [
			"customer" => $name,
		];
		$dynamodb = $this->app['aws']->createDynamoDb();
		$marshaler = new Marshaler();
		$params = [
		    'TableName' => $this->table_name,
		    'Key' => $marshaler->marshalJson(json_encode($arr))
		];
		try {
		    $result = $dynamodb->getItem($params);
		   	if (empty($result['Item'])) {
		   		return false;
		   	}
		    return true;

		} catch (DynamoDbException $e) {
		    $this->error = $e->getMessage();
			return false;
		}
	}

	public function getAccountDetails(string $account): array {
		$this->error = "";
		$arr = [
			"customer" => $account,
		];
		$dynamodb = $this->app['aws']->createDynamoDb();
		$marshaler = new Marshaler();
		$params = [
		    'TableName' => $this->table_name,
		    'Key' => $marshaler->marshalJson(json_encode($arr))
		];
		try {
		    $result = $dynamodb->getItem($params);
		   	if (empty($result['Item'])) {
		   		return [];
		   	}
		    return $marshaler->unmarshalItem($result['Item']);

		} catch (DynamoDbException $e) {
		    $this->error = $e->getMessage();
			return [];
		}
	}

	public function createAccount(array $arr): bool {
		$this->error = "";
		$dynamodb = $this->app['aws']->createDynamoDb();
		$marshaler = new Marshaler();
		$params = [
		    'TableName' => $this->table_name,
		    'Item' => $marshaler->marshalJson(json_encode($arr))
		];
		try {
		    $result = $dynamodb->putItem($params);
			return true;

		} catch (DynamoDbException $e) {
		    $this->error = "Unable to add item: " . $e->getMessage();
		    return false;
		}
	}

}

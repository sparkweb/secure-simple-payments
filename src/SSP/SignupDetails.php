<?php
namespace SSP;

use Silex\Application;
use Symfony\Component\HttpFoundation\Request;

class SignupDetails
{

	public static function load(Request $request, Application $app) {

		//Setup Stripe Actions
		$stripe = new Services\StripeActions($app);
		$stripe->setApiKey($app['my_config']['stripe_access_token']);

		//Setup DynamoDb Mapper
		$dynamodb = new Services\DynamoDbMapping($app);

		//Get Code
		if ($request->get("code")) {
			if (!$stripe->getToken($request->get("code"))) {
				die($stripe->error);
			}
			$redirect_args = [
				"stripe_access_token" => $stripe->result->access_token,
				"stripe_publishable_key" => $stripe->result->stripe_publishable_key,
				"stripe_user_id" => $stripe->result->stripe_user_id,
			];
			return $app->redirect($app['url_generator']->generate('signup_details', $redirect_args));

		//Code Error
		} elseif ($request->get("error")) {
			$error = $request->get("error");
			$error_description = $request->get("error_description");
			echo "<p>Error: code=" . htmlspecialchars($error, ENT_QUOTES) . ", description=" . htmlspecialchars($error_description, ENT_QUOTES) . "</p>\n";
			die;
		}

		//Setup Vars From Querystring
		$twig_vars = [
			"stripe_access_token" => $request->get("stripe_access_token"),
			"stripe_publishable_key" => $request->get("stripe_publishable_key"),
			"stripe_user_id" => $request->get("stripe_user_id"),
		];
		return $app['twig']->render('signup-details.twig', $twig_vars);
	}

}

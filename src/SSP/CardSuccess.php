<?php
namespace SSP;

use Silex\Application;
use Symfony\Component\HttpFoundation\Request;

class CardSuccess
{

	public static function load(Request $request, Application $app) {

		$twig_vars['customer'] = $request->get("customer");
		return $app['twig']->render('card-success.twig', $twig_vars);
	}

}

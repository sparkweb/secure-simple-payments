<?php
namespace SSP;

use Silex\Application;
use Symfony\Component\HttpFoundation\Request;

class Api
{

	public static function load(Request $request, Application $app) {

		//Setup DynamoDb Mapper
		$dynamodb = new Services\DynamoDbMapping($app);

		//Check Headers
		$account = trim($request->headers->get("X-ACCOUNTID"));
		$api_key = trim($request->headers->get("X-APIKEY"));

		//Get Account Details
		$account_details = $dynamodb->getAccountDetails($account);

		//Invalid Login
		if (empty($account_details) || $api_key !== $account_details['api_key']) {
			return $app->json(['message' => "Invalid Login"], 401);
		}

		//Setup Stripe Actions
		$stripe = new Services\StripeActions($app);
		$stripe->setApiKey($account_details['stripe_access_token']);

		//Now Let's Add the Invoice Item
		$content = $request->getContent();
		$data = json_decode($content, true);

		if (empty($data['customer_id'])) {
			return $app->json(['message' => "customer_id missing"], 400);
		} elseif (empty($data['description'])) {
			return $app->json(['message' => "description missing"], 400);
		} elseif (empty($data['amount'])) {
			return $app->json(['message' => "amount missing"], 400);
		}

		//Charge Immediately
		if ($account_details['invoice_frequency'] === "immediately") {
			$shipping_amount = isset($data['shipping']) ? ($data['shipping'] * 100) : 0;
			$amount = $data['amount'] * 100 + $shipping_amount;
			$percentage = $account_details['percentage'] * .01;
			$arg = [
				"amount" => $amount,
				"application_fee" => round($amount * $percentage),
				"currency" => $data['currency'] ?? "usd",
				"customer" => $data['customer_id'],
				"description" => $data['description'],
			];

			if ($stripe->createCharge($arg)) {
				return $app->json(['message' => "Invoice Item Charged Successfully"], 201);
			} else {
				return $app->json(['message' => $stripe->error], 500);
			}
		}

		//Create Invoice Item
		$amount = $data['amount'] * 100;
		$arg = [
			"customer" => $data['customer_id'],
			"amount" => $amount,
			"currency" => $data['currency'] ?? "usd",
			"description" => $data['description'],
		];
		if (!$stripe->createInvoiceItem($arg)) {
			return $app->json(['message' => $stripe->error], 500);
		}

		//Create a Shipping Invoice Item
		if (!empty($data['shipping'])) {
			$shipping_amount = isset($data['shipping']) ? ($data['shipping'] * 100) : 0;
			$arg = [
				"customer" => $data['customer_id'],
				"amount" => $shipping_amount,
				"currency" => $data['currency'] ?? "usd",
				"description" => "Shipping",
			];
			if (!$stripe->createInvoiceItem($arg)) {
				return $app->json(['message' => $stripe->error], 500);
			}
		}

		//All Done
		return $app->json(['message' => "Invoice Item Added Successfully"], 201);
	}

}

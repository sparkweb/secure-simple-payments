<?php
namespace SSP;

use Silex\Application;
use Symfony\Component\HttpFoundation\Request;

class SignupComplete
{

	public static function load(Request $request, Application $app) {

		//Setup DynamoDb Mapper
		$dynamodb = new Services\DynamoDbMapping($app);

		//Setup Account!
		$arr = [
			"customer" => preg_replace('/[^\da-z]/i', '', strtolower($request->get("customer"))),
			"stripe_access_token" => $request->get("stripe_access_token"),
			"stripe_publishable_key" => $request->get("stripe_publishable_key"),
			"stripe_user_id" => $request->get("stripe_user_id"),
			"invoice_frequency" => $request->get("invoice_frequency"),
			"company_name" => $request->get("company_name"),
			"phone" => $request->get("phone"),
			"invoice_frequency" => $request->get("invoice_frequency"),
			"email" => $request->get("email"),
			"name" => $request->get("name"),
			"api_key" => self::createRandom(24),
			"percentage" => 2.5,
		];

		//Make sure they are all filled out
		if (empty($request->files->get("logoupload"))) {
			die("Please upload a logo");
		}
		foreach ($arr as $key => $val) {
			if (empty($val)) {
				die("Whoops, looks like you forgot to fill out a field ({$key}).");
			}
		}

		//Check To See If Taken
		if ($dynamodb->checkForAccount($arr['customer'])) {
			echo "This Account Name Has Already Been Used";
			die;
		}

		//Create Record
		if ($dynamodb->createAccount($arr)) {

			//Setup Stripe Actions
			$stripe = new Services\StripeActions($app);
			$stripe->setApiKey($request->get("stripe_access_token"));

			//Create Stripe Plans
			$stripe->createPlans();

			//Upload Logo to S3
			try {
				$file = $request->files->get("logoupload");
				$stream = fopen($file->getRealPath(), 'r+');
				$app['aws_s3']->putObject([
					'Bucket' => "securesimplepayments",
					'Key'    => 'images/' . $arr['customer'] . ".png",
					'Body'   => $stream,
					'ACL' => 'public-read',
					'ContentType' => 'image/png',
				]);
			} catch(\Exception $e) {
				echo $e->getMessage();
				die;
			}

			$redirect_args = [
				"customer" => $arr['customer'],
				"api_key" => $arr['api_key'],
			];

			return $app->redirect($app['url_generator']->generate('signup_success', $redirect_args));

		//There was an error creating the record
		} else {
			die($mapper->error);
		}
	}


	public static function createRandom($length = 12) {
		$characters = '0123456789abcdefghijklmnopqrstuvwxyzABCDEFGHIJKLMNOPQRSTUVWXYZ';
		$random_string = '';
		for ($i = 0; $i < $length; $i++) {
			$random_string .= $characters[rand(0, strlen($characters) - 1)];
		}
		return $random_string;
	}

}

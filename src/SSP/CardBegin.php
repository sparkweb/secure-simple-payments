<?php
namespace SSP;

use Silex\Application;
use Symfony\Component\HttpFoundation\Request;

class CardBegin
{

	public static function load(Request $request, Application $app) {

		//Setup DynamoDb Mapper
		$dynamodb = new Services\DynamoDbMapping($app);

		//Get Account Details
		$account = $dynamodb->getAccountDetails($request->get("customer"));
		$twig_vars = $account;
		$twig_vars['customer'] = $request->get("customer");

		if (empty($account)) {
			return $app->redirect($app['url_generator']->generate('home'));
		}

		return $app['twig']->render('card-begin.twig', $twig_vars);
	}

}

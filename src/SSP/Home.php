<?php
namespace SSP;

use Silex\Application;
use Symfony\Component\HttpFoundation\Request;

class Home
{

	public static function home(Request $request, Application $app) {

		return $app['twig']->render('home.twig');
	}

}

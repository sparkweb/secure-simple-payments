<?php
namespace SSP\Services;

use Silex\Application;
use Stripe\Stripe;

class StripeActions
{

	private $app;
	public $error;
	public $result;


	public function __construct(Application $app) {
		$this->app = $app;
		Stripe::setClientId($app['my_config']['stripe_client_id']);
	}

	public function setApiKey(string $api_key) {
		Stripe::setApiKey($api_key);
	}

	public function getAuthorizeUrl(): string {
		return \Stripe\OAuth::authorizeUrl([
			'scope' => 'read_write',
			'stripe_landing' => 'login',
		]);
	}

	public function getToken(string $code) {
		$this->error = "";
		try {
			$this->result = \Stripe\OAuth::token([
				'grant_type' => 'authorization_code',
				'code' => $code,
			]);
			return true;
		} catch (\Stripe\Error\OAuth\OAuthBase $e) {
			$this->error = $e->getMessage();
			return false;
		}
	}

	public function createPlans(): bool {
		$this->error = "";
		try {
			$plan = \Stripe\Plan::create([
				"amount" => 0,
				"interval" => "day",
				"product" => [
					"name" => "Regular Service Billing",
				],
				"currency" => "usd",
				"id" => "service-billing-day",
				"nickname" => "Daily Service Billing",
			]);
			$plan = \Stripe\Plan::create([
				"amount" => 0,
				"interval" => "week",
				"product" => [
					"name" => "Regular Service Billing",
				],
				"currency" => "usd",
				"id" => "service-billing-week",
				"nickname" => "Weekly Service Billing",
			]);
			$plan = \Stripe\Plan::create([
				"amount" => 0,
				"interval" => "month",
				"product" => [
					"name" => "Regular Service Billing",
				],
				"currency" => "usd",
				"id" => "service-billing-month",
				"nickname" => "Monthly Service Billing",
			]);
			return true;
		} catch (\Exception $e) {
			$this->error = $e->getMessage();
			return false;
		}
	}

	public function createCustomer(array $arg): bool {
		$this->error = "";
		try {
			$this->result = \Stripe\Customer::create($arg);
			return true;
		} catch (\Exception $e) {
			$this->error = $e->getMessage();
			return false;
		}
	}

	public function createSubscription(array $arg) {
		$this->error = "";
		try {
			$this->result = \Stripe\Subscription::create($arg);
			return true;
		} catch (\Exception $e) {
			$this->error = $e->getMessage();
			return false;
		}
	}

	public function createCharge(array $arg): bool {
		$this->error = "";
		try {
			$this->result = \Stripe\Charge::create($arg);
			return true;
		} catch (\Exception $e) {
			$this->error = $e->getMessage();
			return false;
		}
	}

	public function createInvoiceItem(array $arg): bool {
		$this->error = "";
		try {
			$this->result = \Stripe\InvoiceItem::create($arg);
			return true;
		} catch (\Exception $e) {
			$this->error = $e->getMessage();
			return false;
		}
	}


}

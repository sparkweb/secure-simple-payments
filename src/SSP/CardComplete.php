<?php
namespace SSP;

use Silex\Application;
use Symfony\Component\HttpFoundation\Request;

class CardComplete
{

	public static function load(Request $request, Application $app) {

		//Setup DynamoDb Mapper
		$dynamodb = new Services\DynamoDbMapping($app);

		//Get Account Details
		$account_details = $dynamodb->getAccountDetails($request->get("customer"));
		$twig_vars = $account_details;
		$twig_vars['customer'] = $request->get("customer");
		if (empty($account_details)) {
			return $app->redirect($app['url_generator']->generate('home'));
		}

		//Let's Process This Baby
		if (!$request->get("stripeToken")) {
			die("Sorry! We ran into a problem. Please go back and try again.");
		}

		//Setup Stripe Actions
		$stripe = new Services\StripeActions($app);
		$stripe->setApiKey($account_details['stripe_access_token']);

		//Create Customer With Token
		$arg = [
			"email" => trim($request->get("email")),
			"description" => trim($request->get("name")),
			"source" => $request->get("stripeToken"),
		];
		if (!$stripe->createCustomer($arg)) {
			die($stripe->error);
		}
		$customer = $stripe->result;

		//Set Frequency Options
		$frequency_options = [
			"day" => "tomorrow 2am",
			"week" => "next saturday 2am",
			"year" => "first day of next month 2am",
		];

		//Create Subscription
		if (array_key_exists($account_details['invoice_frequency'], $frequency_options)) {
			$arg = [
				"customer" => $customer->id,
				"application_fee_percent" => $account_details['percentage'],
				"billing" => "charge_automatically",
				"billing_cycle_anchor" => strtotime($frequency_options[$account_details['invoice_frequency']]),
				"items" => [
					[
						"plan" => "service-billing-" . $account_details['invoice_frequency'],
					],
				],
			];
			if (!$stripe->createSubscription($arg)) {
				die($stripe->error);
			}
			$subscription = $stripe->result;

		}

		return $app->redirect($app['url_generator']->generate('card_success', $twig_vars));
	}

}

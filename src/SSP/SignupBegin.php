<?php
namespace SSP;

use Silex\Application;
use Symfony\Component\HttpFoundation\Request;

class SignupBegin
{

	public static function load(Request $request, Application $app) {

		//Setup Stripe Actions
		$stripe = new Services\StripeActions($app);

		//Set Auth URL
		$twig_vars['auth_url'] = $stripe->getAuthorizeUrl();

		return $app['twig']->render('signup-begin.twig', $twig_vars);
	}

}

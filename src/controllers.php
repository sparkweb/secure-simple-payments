<?php
$app->get('/', 'SSP\Home::home')->bind("home");

$app->get('/signup/beta', 'SSP\SignupBegin::load')->bind("signup_begin");
$app->get('/signup/details', 'SSP\SignupDetails::load')->bind("signup_details");
$app->post('/signup/complete', 'SSP\SignupComplete::load')->bind("signup_complete");
$app->get('/signup/success', 'SSP\SignupSuccess::load')->bind("signup_success");

$app->get('/enter-card-details/{customer}', 'SSP\CardBegin::load')->bind("card_begin");
$app->post('/enter-card-details/{customer}/complete', 'SSP\CardComplete::load')->bind("card_complete");
$app->get('/enter-card-details/{customer}/success', 'SSP\CardSuccess::load')->bind("card_success");

$app->post('/api', 'SSP\Api::load')->bind("api");

//All Done!
return $app;
